package org.speech.util;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer;

public class AudioInfo {

	static Mixer.Info[] infos;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		infos = AudioSystem.getMixerInfo();
		for (Mixer.Info info: infos) {
			System.out.println(info.getName());
			System.out.println(info.getVendor());
			System.out.println(info.getVersion());
			System.out.println(info.getDescription());
			System.out.println("*********************"); 
		}
	}

}
