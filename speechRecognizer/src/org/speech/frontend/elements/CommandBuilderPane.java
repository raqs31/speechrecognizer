package org.speech.frontend.elements;

import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.speech.api.CommandHandler;
import org.speech.api.Interceptor;
import org.speech.api.util.ConfigurationFileUtil;

import edu.cmu.sphinx.jsgf.JSGFGrammarException;
import edu.cmu.sphinx.jsgf.JSGFGrammarParseException;

public class CommandBuilderPane extends JPanel {
	private static final long serialVersionUID = 7909019046556542806L;
	
	private class FilePickerButtonAction implements ActionListener {
		private File prevDirectory = null;	// utrzymuje ścieżkę do ostantio wybranego pliku

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser(prevDirectory);
			chooser.setAcceptAllFileFilterUsed(false);
			
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Pliki wykonywalne", "exe");
			chooser.setFileFilter(filter);
			
			int returnVal = chooser.showOpenDialog(bFilePicker);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				prevDirectory = chooser.getSelectedFile();
				filePath = prevDirectory.getPath();
				
				lExecuteFilePath.setText(filePath);
			}
		}
	}
	
	private class CreateCommandButtonAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String key = tfCommandKey.getText();
			String command = tfCommand.getText();
			lastCommandKey = key;
			
			try {
				interceptor.addCommandToGrammar("exec", key);
				cmdHandler.addCommand(key, cmdHandler.createExecutableCommand(filePath, command));
			} catch (IOException | JSGFGrammarParseException | JSGFGrammarException e1) {
				System.out.println("Nie udało się dodać komendy do słownika");
				e1.printStackTrace();
			}
		}
	}
	
	private class DebugButtonAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				cmdHandler.executeCommand(lastCommandKey);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} 
		
	}
	
	private class WriteConfigFileAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			ConfigurationFileUtil.saveConfigFile(cmdHandler);
		}
	}

	
	private CommandHandler cmdHandler = null;
	private Interceptor interceptor = null;
	
	private static JTextField tfCommandKey = null;
	private static JTextField tfCommand	   = null;
	private static JLabel	  lExecuteFilePath = null;
	private static JButton	  bFilePicker = null;
	private static JButton	  bAddCommand = null;
	private static JButton	  bWriteConfigFile = null;
	private static LayoutManager layout = null;
	private static JButton 	  bTest = null;
	
	private String filePath = null;
	private String lastCommandKey = null;
	
	private static final boolean isDebug = true;
	
	
	public CommandBuilderPane(CommandHandler cmdHandler, Interceptor interceptor) {
		super();
		setSize(800, 250);
		setVisible(true);
		layout = new GridLayout(0, 2, 10, 10);
		setLayout(layout);
		
		tfCommandKey = new JTextField();
		tfCommandKey.setColumns(15);
		tfCommandKey.setVisible(true);
		
		tfCommand = new JTextField();
		tfCommand.setColumns(15);
		tfCommand.setVisible(true);
		
		lExecuteFilePath = new JLabel("<< Ścieżka do pliku >>");
		lExecuteFilePath.setSize(200, 25);
		lExecuteFilePath.setVisible(true);
		
		bFilePicker = new JButton("Wybierz");
		bFilePicker.setVisible(true);
		bFilePicker.addActionListener(new FilePickerButtonAction());
		
		bAddCommand = new JButton("Dodaj komendę");
		bAddCommand.setVisible(true);
		bAddCommand.addActionListener(new CreateCommandButtonAction());
		
		bTest = new JButton("Test last command");
		bTest.setVisible(isDebug);
		bTest.addActionListener(new DebugButtonAction());
		
		bWriteConfigFile = new JButton("Zapisz plik konfiguracyjny");
		bWriteConfigFile.setVisible(true);
		bWriteConfigFile.addActionListener(new WriteConfigFileAction());
		
		add(lExecuteFilePath);
		add(bFilePicker);
		add(new JLabel("Komenda:"));
		add(tfCommand);
		add(new JLabel("Klucz komendy:"));
		add(tfCommandKey);
		add(bAddCommand);
		add(bTest);
		add(bWriteConfigFile);
		
		this.cmdHandler = cmdHandler;
		this.interceptor = interceptor;
	}
	
	public String getFilePath() {
		return filePath;
	}
	
	public String getCommandKey() {
		return tfCommandKey.getText();
	}
	
	public String getCommand() {
		return tfCommand.getText();
	}
}