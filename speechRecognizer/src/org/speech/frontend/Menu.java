package org.speech.frontend;

import java.awt.Color;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.speech.api.CommandHandler;
import org.speech.api.Interceptor;
import org.speech.api.conf.InterceptorManager;
import org.speech.api.win.WindowsCommandHandler;
import org.speech.frontend.elements.CommandBuilderPane;

import edu.cmu.sphinx.jsgf.JSGFGrammarException;
import edu.cmu.sphinx.jsgf.JSGFGrammarParseException;


/**
 * 
 * @author Mario
 *
 */
public class Menu extends JFrame {
	private static final long serialVersionUID = 5272578409051327250L;
	private static JPanel panel = null;
	private static CommandBuilderPane commandBuilderPane = null;
	private static Interceptor interceptor = null;
	private static CommandHandler handler = null;
	
	public Menu() {
		super("Speech Recognizer Menu");
		setSize(800, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		panel = new JPanel();
		add(panel);

		panel.setAutoscrolls(true);
		panel.setVisible(true);
		panel.setLayout(null);
		panel.setBackground(Color.DARK_GRAY);
		
		handler = WindowsCommandHandler.getCommandHandler();
		handler.readConfigFile();
		
		interceptor = InterceptorManager.getInterceptorManager();
		interceptor.start();
		try {
			interceptor.addListToGrammar("exec", handler.getCommands());
		} catch (IOException | JSGFGrammarParseException | JSGFGrammarException e) {
			e.printStackTrace();
		}
		
		commandBuilderPane = new CommandBuilderPane(handler, interceptor);
		panel.add(commandBuilderPane);
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Menu menu = new Menu();
				menu.setVisible(true);
			}
		});
	}
}
