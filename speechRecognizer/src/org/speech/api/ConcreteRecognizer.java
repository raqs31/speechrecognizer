package org.speech.api;

import edu.cmu.sphinx.util.props.ConfigurationManager;

public interface ConcreteRecognizer extends Runnable {
	public void start();
	public void stop();
	public boolean isRun();
//	public ConcreteRecognizer getRecognizer(ConfigurationManager cm);
}
