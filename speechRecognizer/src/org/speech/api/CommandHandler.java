package org.speech.api;

import java.io.IOException;
import java.util.List;

import org.speech.exec.Command;

public interface CommandHandler extends ConcreteInterceptors {
	public void readConfigFile();
	
	public void addCommand(String key, String cmdExec);
	
	public void addCommand(String key);
	
	public void printAllCommand();
	
	public String executeCommand(String commandKey) throws IOException;
	
	public String exec(String command) throws IOException;
	
	public String executeCommand(Command cmd) throws IOException;
	
	public String createExecutableCommand(String fp, String cmd);
	
	public List<Command> getCommands(); 
	
	public String getConfigurationFilePath();
}
