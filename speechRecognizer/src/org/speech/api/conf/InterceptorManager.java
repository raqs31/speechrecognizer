package org.speech.api.conf;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.random.ISAACRandom;
import org.speech.api.CommandHandler;
import org.speech.api.ConcreteRecognizer;
import org.speech.api.Interceptor;
import org.speech.api.win.WindowsCommandHandler;
import org.speech.exec.Command;
import org.speech.exec.com.CommandDAO;

import edu.cmu.sphinx.frontend.util.Microphone;
import edu.cmu.sphinx.jsgf.JSGFGrammar;
import edu.cmu.sphinx.jsgf.JSGFGrammarException;
import edu.cmu.sphinx.jsgf.JSGFGrammarParseException;
import edu.cmu.sphinx.jsgf.JSGFRuleGrammar;
import edu.cmu.sphinx.jsgf.JSGFRuleGrammarManager;
import edu.cmu.sphinx.jsgf.rule.JSGFRule;
import edu.cmu.sphinx.jsgf.rule.JSGFRuleAlternatives;
import edu.cmu.sphinx.jsgf.rule.JSGFRuleToken;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;

public class InterceptorManager extends Interceptor {
	private static final String CONFIGURATION_FILE = "resources/Configuration.xml";
	
	private static Interceptor interceptor = null;
	
	private ConfigurationManager cm = null;
	private CommandHandler handler = null;
	private ConcreteRecognizer rec = null;
	private Executor executor = null;
	
	private InterceptorManager() {
		cm = new ConfigurationManager(CONFIGURATION_FILE);
		handler = WindowsCommandHandler.getCommandHandler();
		handler.readConfigFile();
		
		if (isDebug) handler.printAllCommand();
		
		Recognizer recognizer = (Recognizer) cm.lookup("recognizer");
		recognizer.allocate();
		
		rec = this.new MicrophoneWatcher(cm);
		executor = Executors.newSingleThreadExecutor();
		executor.execute(rec);
	}
	
	public static Interceptor getInterceptorManager() {
		if (interceptor == null)
			interceptor = new InterceptorManager();
		return interceptor;
	}
	
	@Override
	public void addListToGrammar(String token, List<Command> cmdList) throws IOException, JSGFGrammarParseException, JSGFGrammarException {
		JSGFGrammar gr = (JSGFGrammar) cm.lookup("jsgfGrammar");
		JSGFRuleGrammar ruleGr = gr.getRuleGrammar();
		List<JSGFRule> ruleList = new LinkedList<JSGFRule>();
		
		for (Command c: cmdList) {
			if (StringUtils.isNotBlank(c.getKey())) {
				ruleList.add(new JSGFRuleToken(c.getKey()));
			}
		}
		if (ruleList.size() > 0) {
			List<JSGFRule> seqList = new LinkedList<JSGFRule>();
			seqList.add(new JSGFRuleAlternatives(ruleList));
			
			ruleGr.setRule(token, new JSGFRuleAlternatives(seqList), true);
			
			gr.commitChanges();
		}
		if (isDebug) System.out.println(ruleList);

	}

	@Override
	public void addCommandToGrammar(String ruleName, String word) throws IOException, JSGFGrammarParseException, JSGFGrammarException {
		JSGFGrammar gr = (JSGFGrammar) cm.lookup("jsgfGrammar");
		JSGFRuleGrammar ruleGr = gr.getRuleGrammar();
		JSGFRule rule = null;
		
		if ((rule = ruleGr.getRule(ruleName)) != null) {
			JSGFRuleAlternatives ruleAlt = (JSGFRuleAlternatives)rule;
			List<JSGFRule> ruleList = ruleAlt.getRules();
			ruleList.add(new JSGFRuleToken(word));
			ruleAlt.setRules(ruleList);
		} else {
			rule = new JSGFRuleToken(word);
			List<JSGFRule> ruleList = new LinkedList<JSGFRule>();
			List<JSGFRule> seqList = new LinkedList<JSGFRule>();
			
			ruleList.add(rule);
			seqList.add(new JSGFRuleAlternatives(ruleList));
			ruleGr.setRule(ruleName, new JSGFRuleAlternatives(seqList), true);
		}
		gr.commitChanges();

	}

	public static void main(String[] args) {
		InterceptorManager ic = new InterceptorManager();
		List<Command> cmdToPut = new LinkedList<Command>();
		cmdToPut.add(new CommandDAO("my", "my"));
		cmdToPut.add(new CommandDAO("house", "ad"));
		cmdToPut.add(new CommandDAO("is", "takie tam3"));
		cmdToPut.add(new CommandDAO("big", "takie tam4"));
		
		try {
			ic.addListToGrammar("mój token", cmdToPut);
		} catch (IOException | JSGFGrammarParseException | JSGFGrammarException e) {
			e.printStackTrace();
		}
		try {
			ic.addCommandToGrammar("mój token", "back");
		} catch (Exception e) {
			e.printStackTrace();
		}
		ic.start();
	}
	
	private class MicrophoneWatcher implements ConcreteRecognizer {
		private Recognizer rc = null;
		private Microphone mic = null;
		private Robot robot = null;
		
		private boolean isRun = false;
		private boolean isScreenEventActive = true;
		private MicrophoneWatcher() {}
		
		private MicrophoneWatcher(ConfigurationManager cm) {
			rc = (Recognizer)cm.lookup("recognizer");
			mic = cm.lookup("microphone");
			try {
				robot = new Robot();
			} catch (AWTException e) {
				isScreenEventActive = false;
				e.printStackTrace();
			}
		}
		
		@Override
		public void run() {
			while (true) {
				recognize();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		}
		@Override
		public void start() {
			isRun = true;
		}

		@Override
		public void stop() {
			isRun = false;
		}

		@Override
		public boolean isRun() {
			return isRun;
		}
		
		private void recognize() {
			while (isRun) {
				if (!mic.isRecording())	
					if (!mic.startRecording()) {
						System.out.println("cannot start microphone");
							return;
				}
				System.out.println("Record");
				Result result = rc.recognize();
				
				if (result != null) {
					String resultText = result.getBestResultNoFiller();
					
					System.out.println("You said: " +  resultText);
					if (resultText.length() == 1 ) {
						if (isScreenEventActive)
							type(CharUtils.toChar(resultText));
					} else {
						try {
							handler.executeCommand(resultText);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
			mic.stopRecording();
		}
		public void type(char character) {
	        switch (character) {
		        case 'a': doType(KeyEvent.VK_A); break;
		        case 'b': doType(KeyEvent.VK_B); break;
		        case 'c': doType(KeyEvent.VK_C); break;
		        case 'd': doType(KeyEvent.VK_D); break;
		        case 'e': doType(KeyEvent.VK_E); break;
		        case 'f': doType(KeyEvent.VK_F); break;
		        case 'g': doType(KeyEvent.VK_G); break;
		        case 'h': doType(KeyEvent.VK_H); break;
		        case 'i': doType(KeyEvent.VK_I); break;
		        case 'j': doType(KeyEvent.VK_J); break;
		        case 'k': doType(KeyEvent.VK_K); break;
		        case 'l': doType(KeyEvent.VK_L); break;
		        case 'm': doType(KeyEvent.VK_M); break;
		        case 'n': doType(KeyEvent.VK_N); break;
		        case 'o': doType(KeyEvent.VK_O); break;
		        case 'p': doType(KeyEvent.VK_P); break;
		        case 'q': doType(KeyEvent.VK_Q); break;
		        case 'r': doType(KeyEvent.VK_R); break;
		        case 's': doType(KeyEvent.VK_S); break;
		        case 't': doType(KeyEvent.VK_T); break;
		        case 'u': doType(KeyEvent.VK_U); break;
		        case 'v': doType(KeyEvent.VK_V); break;
		        case 'w': doType(KeyEvent.VK_W); break;
		        case 'x': doType(KeyEvent.VK_X); break;
		        case 'y': doType(KeyEvent.VK_Y); break;
		        case 'z': doType(KeyEvent.VK_Z); break;
		        case 'A': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_A); break;
		        case 'B': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_B); break;
		        case 'C': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_C); break;
		        case 'D': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_D); break;
		        case 'E': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_E); break;
		        case 'F': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_F); break;
		        case 'G': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_G); break;
		        case 'H': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_H); break;
		        case 'I': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_I); break;
		        case 'J': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_J); break;
		        case 'K': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_K); break;
		        case 'L': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_L); break;
		        case 'M': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_M); break;
		        case 'N': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_N); break;
		        case 'O': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_O); break;
		        case 'P': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_P); break;
		        case 'Q': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_Q); break;
		        case 'R': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_R); break;
		        case 'S': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_S); break;
		        case 'T': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_T); break;
		        case 'U': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_U); break;
		        case 'V': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_V); break;
		        case 'W': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_W); break;
		        case 'X': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_X); break;
		        case 'Y': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_Y); break;
		        case 'Z': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_Z); break;
		        case '`': doType(KeyEvent.VK_BACK_QUOTE); break;
		        case '0': doType(KeyEvent.VK_0); break;
		        case '1': doType(KeyEvent.VK_1); break;
		        case '2': doType(KeyEvent.VK_2); break;
		        case '3': doType(KeyEvent.VK_3); break;
		        case '4': doType(KeyEvent.VK_4); break;
		        case '5': doType(KeyEvent.VK_5); break;
		        case '6': doType(KeyEvent.VK_6); break;
		        case '7': doType(KeyEvent.VK_7); break;
		        case '8': doType(KeyEvent.VK_8); break;
		        case '9': doType(KeyEvent.VK_9); break;
		        case '-': doType(KeyEvent.VK_MINUS); break;
		        case '=': doType(KeyEvent.VK_EQUALS); break;
		        case '~': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_BACK_QUOTE); break;
		        case '!': doType(KeyEvent.VK_EXCLAMATION_MARK); break;
		        case '@': doType(KeyEvent.VK_AT); break;
		        case '#': doType(KeyEvent.VK_NUMBER_SIGN); break;
		        case '$': doType(KeyEvent.VK_DOLLAR); break;
		        case '%': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_5); break;
		        case '^': doType(KeyEvent.VK_CIRCUMFLEX); break;
		        case '&': doType(KeyEvent.VK_AMPERSAND); break;
		        case '*': doType(KeyEvent.VK_ASTERISK); break;
		        case '(': doType(KeyEvent.VK_LEFT_PARENTHESIS); break;
		        case ')': doType(KeyEvent.VK_RIGHT_PARENTHESIS); break;
		        case '_': doType(KeyEvent.VK_UNDERSCORE); break;
		        case '+': doType(KeyEvent.VK_PLUS); break;
		        case '\t': doType(KeyEvent.VK_TAB); break;
		        case '\n': doType(KeyEvent.VK_ENTER); break;
		        case '[': doType(KeyEvent.VK_OPEN_BRACKET); break;
		        case ']': doType(KeyEvent.VK_CLOSE_BRACKET); break;
		        case '\\': doType(KeyEvent.VK_BACK_SLASH); break;
		        case '{': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_OPEN_BRACKET); break;
		        case '}': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_CLOSE_BRACKET); break;
		        case '|': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_BACK_SLASH); break;
		        case ';': doType(KeyEvent.VK_SEMICOLON); break;
		        case ':': doType(KeyEvent.VK_COLON); break;
		        case '\'': doType(KeyEvent.VK_QUOTE); break;
		        case '"': doType(KeyEvent.VK_QUOTEDBL); break;
		        case ',': doType(KeyEvent.VK_COMMA); break;
		        case '<': doType(KeyEvent.VK_LESS); break;
		        case '.': doType(KeyEvent.VK_PERIOD); break;
		        case '>': doType(KeyEvent.VK_GREATER); break;
		        case '/': doType(KeyEvent.VK_SLASH); break;
		        case '?': doType(KeyEvent.VK_SHIFT, KeyEvent.VK_SLASH); break;
		        case ' ': doType(KeyEvent.VK_SPACE); break;
		        default: throw new IllegalArgumentException("Cannot type character " + character);
	         }
	    }

	    private void doType(int... keyCodes) {
	        doType(keyCodes, 0, keyCodes.length);
	    }

	    private void doType(int[] keyCodes, int offset, int length) {
	        if (length == 0) {
	                return;
	        }

	        robot.keyPress(keyCodes[offset]);
	        doType(keyCodes, offset + 1, length - 1);
	        robot.keyRelease(keyCodes[offset]);
	    }
	}

	@Override
	public void start() {
		rec.start();
		
	}

	@Override
	public void stop() {
		rec.stop();
	}
}
