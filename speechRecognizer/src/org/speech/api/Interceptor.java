package org.speech.api;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.speech.api.win.WindowsCommandHandler;
import org.speech.exec.Command;

import edu.cmu.sphinx.frontend.util.Microphone;
import edu.cmu.sphinx.jsgf.JSGFGrammar;
import edu.cmu.sphinx.jsgf.JSGFGrammarException;
import edu.cmu.sphinx.jsgf.JSGFGrammarParseException;
import edu.cmu.sphinx.jsgf.JSGFRuleGrammar;
import edu.cmu.sphinx.jsgf.rule.JSGFRule;
import edu.cmu.sphinx.jsgf.rule.JSGFRuleAlternatives;
import edu.cmu.sphinx.jsgf.rule.JSGFRuleToken;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;

public abstract class Interceptor implements ConcreteInterceptors {

	private static Logger logger = Logger.getLogger(Interceptor.class.getName());
	
	public static void main(String[] args) throws IOException, AWTException {
		logger.info("Start configure and recognize speech");

		ConfigurationManager cm;
		Robot rob = new Robot();

		boolean quit = false;
        cm = new ConfigurationManager("resources/Configuration.xml");

        CommandHandler handler = WindowsCommandHandler.getCommandHandler();
        handler.readConfigFile();
        handler.printAllCommand();
        
		Recognizer recognizer = (Recognizer) cm.lookup("recognizer");
        recognizer.allocate();
        /* sample how to add additional sentence to grammar */
        JSGFGrammar grammar = (JSGFGrammar)cm.lookup("jsgfGrammar");
    	JSGFRuleGrammar ruleGrammar = grammar.getRuleGrammar();
    	List<JSGFRule> ruleList = new LinkedList<JSGFRule>();
    	ruleList.add(new JSGFRuleToken("money"));
    	ruleList.add(new JSGFRuleToken("happy"));
    	List<JSGFRule> seqList = new LinkedList<JSGFRule>();
    	seqList.add(new JSGFRuleAlternatives(ruleList));
    	ruleGrammar.setRule("taka",  new JSGFRuleAlternatives(seqList), true);

    	try {
			grammar.commitChanges();
		} catch (JSGFGrammarParseException | JSGFGrammarException e) {
			e.printStackTrace();
		}
        // start the microphone or exit if the program if this is not possible
        Microphone microphone = (Microphone) cm.lookup("microphone");
        if (!microphone.startRecording()) {
            System.out.println("Cannot start microphone.");
            recognizer.deallocate();
            System.exit(1);
        }        
        System.out.println("Start speaking. Say quit to exit");
        // loop the recognition until the program exits.
        while (!quit) {
            Result result = recognizer.recognize();

            if (result != null) {
                String resultText = result.getBestFinalResultNoFiller();

                System.out.println("You said: " +  resultText);
                if ("quit".equals(resultText))
                	quit = true;
                else if (resultText.length() == 1) {
                	rob.keyPress(KeyEvent.VK_A);
                }
            } else {
                System.out.println("I can't hear what you said.");
            }
        }
        System.out.println("Thanks for using it");
        recognizer.deallocate();
        System.exit(1);
	}
	
	public abstract void addListToGrammar(String token, List<Command> cmdList) throws IOException, JSGFGrammarParseException, JSGFGrammarException;
	
	public abstract void addCommandToGrammar(String ruleName, String word) throws IOException, JSGFGrammarParseException, JSGFGrammarException;
	
	public void addCommandToGrammar(String token, Command cmd) throws IOException, JSGFGrammarParseException, JSGFGrammarException {
		addCommandToGrammar(token, cmd.getKey());
	}
	
	public abstract void start();
	
	public abstract void stop();
}
