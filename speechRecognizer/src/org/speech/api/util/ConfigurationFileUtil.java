package org.speech.api.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.speech.api.CommandHandler;
import org.speech.api.ConcreteInterceptors;
import org.speech.api.win.WindowsCommandHandler;
import org.speech.exec.Command;
import org.speech.exec.com.CommandDAO;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ConfigurationFileUtil implements ConcreteInterceptors {
	
	public static void main(String[] args) {
		// Test
		CommandHandler handler = WindowsCommandHandler.getCommandHandler(readXmlConfigFile(WindowsCommandHandler.getHomePath()));
		handler.addCommand("takta");
		saveConfigFile(handler);
		
		System.out.println(readXmlConfigFile(handler.getConfigurationFilePath()));
	}
	
	public static Map<String, Command> readXmlConfigFile(String filePath) {
		File xmlFile = new File(filePath);
		Map<String, Command> collection = new HashMap<String, Command>();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			
			if (!xmlFile.exists())
				xmlFile.createNewFile();
				
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			
			NodeList root = doc.getElementsByTagName("Cmd");
			
			for (int i = 0; i < root.getLength(); i++) {
				Node node = root.item(i);
				
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					NodeList child = node.getChildNodes();
				
					for (int j = 0; j < child.getLength(); j++) {
						if(child.item(j).getNodeType() == Node.ELEMENT_NODE) {
							NamedNodeMap attributes = child.item(j).getAttributes();
							Node key = attributes.getNamedItem("id");
							Node cmd = attributes.getNamedItem("value");
							
							Command command = new CommandDAO(key.getTextContent(), cmd.getTextContent());
							collection.put(command.getKey(), command);
						}
					}
				}
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return collection;
	}
	
	public static void saveConfigFile(CommandHandler handler) {
		if (handler == null)
			return;
		
		String homePath = handler.getConfigurationFilePath();
		File confFile = new File(homePath);
		
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder;
		
			docBuilder = docFactory.newDocumentBuilder();
			
			Document doc = docBuilder.newDocument();
			Element root = doc.createElement("Recognizer");
			
			doc.appendChild(root);

			List<Command> commands = handler.getCommands();
			for (Command x : commands) {
				Element cmd = doc.createElement("Cmd");
				Element key = doc.createElement("Key");
				key.setAttribute("id", x.getKey());
				key.setAttribute("value", x.getCommand());
				cmd.appendChild(key);
				root.appendChild(cmd);
			}
			
			TransformerFactory trFactory = TransformerFactory.newInstance();
			trFactory.setAttribute("indent-number", new Integer(2));
			
			Transformer transformer = trFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(confFile);
			
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}
}
