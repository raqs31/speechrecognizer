package org.speech.api.win;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.speech.api.CommandHandler;
import org.speech.api.ConcreteInterceptors;
import org.speech.api.util.ConfigurationFileUtil;
import org.speech.exec.Command;
import org.speech.exec.CommandCollection;
import org.speech.exec.com.CommandDAO;
import org.speech.exec.com.SimpleCommandHashMapCollection;

public class WindowsCommandHandler implements CommandHandler {
	public static Logger logger = Logger.getLogger(WindowsCommandHandler.class.toString());
	
	// Singleton
	private static CommandHandler handler = null;
	
	private static final String CONFIG_FILE = "\\Recognizer\\Config.xml";
	
	private CommandCollection cmdCollection = null;
	
	private WindowsCommandHandler() {
		cmdCollection = new SimpleCommandHashMapCollection();
	}
	
	private WindowsCommandHandler(Map<String, Command> collection) {
		cmdCollection = new SimpleCommandHashMapCollection(collection);
	}
	
	public static CommandHandler getCommandHandler() {
		if (handler == null)
			handler = new WindowsCommandHandler();
		return handler;
	}
	
	public static CommandHandler getCommandHandler(Map<String, Command> cmdCollection) {
		if (handler == null)
			handler = new WindowsCommandHandler(cmdCollection);
		else 
			cmdCollection.putAll(cmdCollection);
		
		return handler;
	}
	

	@Override
	public void readConfigFile() {
		Map<String, Command> commandsMap = ConfigurationFileUtil.readXmlConfigFile(getConfigurationFilePath());
		cmdCollection.addCollection(commandsMap);
	}
	
	public static String getHomePath() {
		return System.getProperty("user.home") + CONFIG_FILE;
	}

	public static void main(String[] args) {
		try {
			Process p = Runtime.getRuntime().exec("cmd /C \"C:\\Program Files (x86)\\AIMP3\\AIMP3.exe\" /NEXT");

			BufferedReader stdIn = new BufferedReader(new InputStreamReader(p.getInputStream()), 8*1024);

			String s = null;
			System.out.println("Here is the standard ouut of the command:\n");
			while ((s = stdIn.readLine()) != null) 
			System.out.println(s);
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void addCommand(String key, String cmdExec) {
		Command cmd = null;
		
		if (key == null)
			return;
		cmd = new CommandDAO(key, cmdExec);
		
		if (cmdCollection.addCommand(cmd))
			logger.info("Command " + cmd + " added");
		else
			logger.warning("Error while add command: " + cmd);
	}

	@Override
	public void addCommand(String key) {
		Command cmd = null;
		
		if (key == null)
			return;
		cmd = new CommandDAO(key);
		
		if (cmdCollection.addCommand(cmd))
			logger.info("Command " + cmd + " added");
		else
			logger.warning("Error while add command: " + cmd);
	}
	@Override
	public void printAllCommand() {
		cmdCollection.printAll();
	}

	@Override
	public String executeCommand(String commandKey) throws IOException {
		logger.info("CommandKey: " + commandKey);
		Command cmd = null;
		if (commandKey != null && (cmd = cmdCollection.getCommand(commandKey)) != null)
			return executeCommand(cmd);
		return null;
	}

	@Override
	public String exec(String command) throws IOException {
		try {
			logger.info("Command: " + command);
			Process process = Runtime.getRuntime().exec("cmd /C " + command);
/*
			BufferedReader stdIn = new BufferedReader(new InputStreamReader(process.getInputStream()), 2048);
			StringBuilder sb = new StringBuilder();
			String line = stdIn.readLine();
			
			if(line != null)
				sb.append(line);

			System.out.println("After");
			
			return sb.length() > 0 ? sb.toString() : null;
*/
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public String executeCommand(Command cmd) throws IOException {
		logger.info("Command: " + cmd);
		if (cmd != null && cmd.getCommand() != null)
			return exec(cmd.getCommand());
		return null;
	}

	@Override
	public String createExecutableCommand(String fp, String cmd) {
		StringBuilder sb = new StringBuilder("\"");
		if (fp == null)
			return null;
		
		sb.append(fp);
		sb.append("\" ");
		sb.append(cmd);
		
		return sb.toString();
	}
	
	@Override
	public List<Command> getCommands() {
		return cmdCollection.getCommands();
	}

	@Override
	public String getConfigurationFilePath() {
		return getHomePath();
	}
}
