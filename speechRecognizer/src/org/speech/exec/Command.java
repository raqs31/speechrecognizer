package org.speech.exec;

public interface Command {
	String getKey();
	
	void setCommand(String command);
	
	String getCommand();
}
