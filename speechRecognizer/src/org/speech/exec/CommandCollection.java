package org.speech.exec;

import java.util.List;
import java.util.Map;

public interface CommandCollection {
	
	Command getCommand(String key);
	
	boolean addCommand(Command command);
	
	Command removeCommand(Command command);

	Command removeCommand(String key);
	
	void printAll();
	
	void addCollection(Map<String, Command> collection);
	
	List<Command> getCommands();
}
