package org.speech.exec.com;

import org.speech.exec.Command;

public class CommandDAO implements Command {

	private String key = null;
	private String command = null;
	
	/**
	 * Stub for default constructor
	 */
	@SuppressWarnings("unused")
	private CommandDAO() {};
	
	public CommandDAO(String key) {
		this.key = key;
		this.command = null;
	}
	
	public CommandDAO(String key, String command) {
		this.key = key;
		this.command = command;
	}
	  
	@Override
	public String getKey() {
		return key;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public String getCommand() {
		return command;
	}
	
	@Override
	public String toString() {
		return "[Command key: " + key + " exec: " + command + "]";
	}

}
