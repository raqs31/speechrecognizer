package org.speech.exec.com;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.RowFilter.Entry;

import org.speech.exec.Command;
import org.speech.exec.CommandCollection;

public class SimpleCommandHashMapCollection implements CommandCollection {
	private Map<String, Command> collection;
	
	public SimpleCommandHashMapCollection() {
		collection = new HashMap<String, Command>();
	}
	
	public SimpleCommandHashMapCollection(Map<String, Command> contains) {
		collection = new HashMap<String, Command>(contains);
	}

	@Override
	public Command getCommand(String key) {
		return collection.get(key);
	}

	@Override
	public boolean addCommand(Command command) {
		if (command != null && command.getKey() != null && !collection.containsKey(command.getKey())) {
			collection.put(command.getKey(), command);
			return true;
		}
		return false;
	}

	@Override
	public Command removeCommand(Command command) {
		if (command == null || command.getKey() == null) 
			return null;
		else 
			return collection.remove(command);
	}

	@Override
	public Command removeCommand(String key) {
		if (key != null)
			return collection.remove(key);
		return null;
	}

	@Override
	public void printAll() {
		Collection<Command> allCommand = collection.values();
		System.out.println("============ Contained Commands ===========");
		for (Command cmd: allCommand) {
			System.out.println(cmd);
		}
		System.out.println("================================ ===========");
	}
	@Override
	public void addCollection(Map<String, Command> collection) {
		this.collection.putAll(collection);
	}

	@Override
	public List<Command> getCommands() {
		return new LinkedList<Command>(collection.values());
	}

}
